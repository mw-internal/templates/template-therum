<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" <?php print lang_attributes(); ?>>

<head>
    <title>{content_meta_title}</title>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <link href="https://fonts.cdnfonts.com/css/milliard" rel="stylesheet">

    <script>
        mw.require('icon_selector.js');
     //  mw.lib.require('microweber_ui');
     //   mw.require('<?php //print template_url(); ?>//assets/plugins/mw-ui/grunt/plugins/ui/css/main.css');
     //   mw.require('<?php //print template_url(); ?>//assets/plugins/mw-ui/assets/ui/plugins/css/plugins.min.css');
        mw.require('<?php print template_url(); ?>assets/plugins/mw-ui/assets/ui/plugins/js/plugins.js');

        mw.iconLoader()
            .addIconSet('materialDesignIcons')
    </script>



    <?php print get_template_stylesheet(); ?>

    <link href="<?php print template_url(); ?>assets/css/main.css" rel="stylesheet"/>

    <?php include('template_settings.php'); ?>
</head>
<body class="<?php print helper_body_classes(); ?> <?php print $sticky_navigation; ?> ">

<?php if ($preloader == 'true'): ?>
    <div class="js-ajax-loading">
        <module type="logo" id="header-logo-loading" logo-name="header-logo" class="w-100"/>
    </div>
<?php endif; ?>

<div class="main">
    <div class="navigation-holder">
        <?php if ($header_style == 'header_style_1'): ?>
            <?php include('partials/header/header_style_1.php'); ?>
        <?php elseif ($header_style == 'header_style_2'): ?>
            <?php include('partials/header/header_style_2.php'); ?>
        <?php elseif ($header_style == 'header_style_3'): ?>
            <?php include('partials/header/header_style_3.php'); ?>
        <?php elseif ($header_style == 'header_style_4'): ?>
            <?php include('partials/header/header_style_4.php'); ?>
        <?php else: ?>
            <?php include('partials/header/header_style_2.php'); ?>
        <?php endif; ?>
    </div>

