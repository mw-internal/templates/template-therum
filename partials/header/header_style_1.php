<nav class="navbar navbar-expand-lg navbar-light header-background header-style-1">
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center w-100">
            <div class="col order-1 order-lg-1" style="max-width: 33%;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-text header-menu-toggle-button"></span>
                </button>
            </div>

            <div class="col-12 col-lg order-4 order-lg-2">
                <div class=" collapse navbar-collapse position-absolute position-lg-relative top-0 bottom-0 w-100" style="z-index: 9;" id="navbarSupportedContent">
                    <module type="menu" name="header_menu" id="header_menu" template="navbar" class="nav-holder"/>
                </div>
            </div>

            <div class="col text-center order-2 order-lg-3">
                <style>.navbar-brand {
                        margin: 0;
                    }</style>
                <module type="logo" id="header-logo" class="mx-auto"/>
            </div>

            <div class="col order-3 order-lg-4">
                <ul class="nav nav-navbar align-items-center justify-content-end">
                    <?php include('parts/search_bar.php'); ?>
                    <?php include('parts/shopping_cart.php'); ?>
                    <?php include('parts/profile_link.php'); ?>
                    <?php include('parts/contact.php'); ?>
                    <?php include('parts/socials.php'); ?>
                    <?php include('parts/phone.php'); ?>
                </ul>
            </div>
        </div>
    </div>
</nav>

