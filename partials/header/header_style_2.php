<nav class="navbar navbar-expand-lg navbar-light header-background header-style-3">
    <div class="container-fluid">
        <div class="row col-12 d-flex justify-content-center ">
            <div class="d-block d-lg-none py-2 mw-big-navbar-toggler order-3 order-lg-1 d-flex justify-content-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-text header-menu-toggle-button"></span>
                </button>

            </div>
            <module type="logo" id="header-logo" class="d-flex col-auto mw-big-header-logo w-auto"/>

            <div class="col-12 col-md order-3 order-lg-2 text-center justify-content-center d-flex">
                <div class="collapse navbar-collapse top-0 bottom-0 justify-content-center" id="navbarSupportedContent" style="z-index: 9;">
                    <module type="menu" name="header_menu" id="header_menu" template="navbar" class="nav-holder"/>
                </div>
            </div>

            <div class="col-auto d-flex order-2 order-lg-3 justify-content-md-end justify-content-center">
                <ul class="nav nav-navbar align-items-center justify-content-md-end justify-content-center">
                    <?php include('parts/search_bar.php'); ?>
                    <?php include('parts/shopping_cart.php'); ?>
                    <?php include('parts/profile_link.php'); ?>
                    <?php include('parts/contact.php'); ?>
                    <?php include('parts/socials.php'); ?>
                    <?php include('parts/phone.php'); ?>
                </ul>
            </div>
        </div>
    </div>
</nav>

