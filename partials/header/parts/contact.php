<?php if ($contact_us_link == 'true'): ?>
    <li class="nav-item dropdown btn-contacts ps-3">
        <div class="edit nodrop safe-mode" field="header_contact_us" rel="global">
            <module type="btn" button_style="btn-primary" button_size="btn-sm px-4" button_text="CONTACT US" />
        </div>
    </li>
<?php endif; ?>
