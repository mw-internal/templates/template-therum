<?php if ($header_socials == 'true'): ?>
    <li class="nav-item dropdown btn-socials ps-3">
        <div class="edit nodrop safe-mode" field="header_socials" rel="global">
            <module type="social_links" template="skin-1" />
        </div>
    </li>
<?php endif; ?>
