<?php if ($phone_text == 'true'): ?>
    <li class="nav-item dropdown btn-phone ps-3">
        <div class="edit nodrop safe-mode" field="header_phone_text" rel="global">
            <span class="text-outline-primary font-weight-bold">00359 878 123 456</span>
        </div>
    </li>
<?php endif; ?>
