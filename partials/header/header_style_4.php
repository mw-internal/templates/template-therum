<nav class="navbar navbar-expand-lg navbar-light header-background header-style-4">
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center w-100">
            <div class="col order-1 order-lg-1">
                <button class="navbar-toggler me-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-text header-menu-toggle-button"></span>
                </button>
                <module type="logo" id="header-logo" class="d-inline-block"/>
            </div>

            <div class="col-12 col-lg-auto order-3 order-lg-2 text-lg-end">
                <div class="collapse navbar-collapse position-absolute top-0 bottom-0" id="navbarSupportedContent" style="z-index: 9;">
                    <module type="menu" name="header_menu" id="header_menu" template="navbar" class="nav-holder"/>
                </div>
            </div>

            <div class="col order-2 order-lg-3">
                <ul class="nav nav-navbar align-items-center justify-content-end">
                    <?php include('parts/search_bar.php'); ?>
                    <?php include('parts/shopping_cart.php'); ?>
                    <?php include('parts/profile_link.php'); ?>
                    <?php include('parts/contact.php'); ?>
                    <?php include('parts/socials.php'); ?>
                    <?php include('parts/phone.php'); ?>
                </ul>
            </div>
        </div>
    </div>
</nav>

