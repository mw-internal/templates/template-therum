<?php if ($footer == 'true'): ?>
    <footer class="footer footer-background p-5">
        <div class="container-fluid">
            <div class="row gap-y align-items-center edit nodrop safe-mode" field="footer" rel="global">
                <div class="col-md-3 text-center text-md-start">
                    <!--                                <a href="--><?php //echo site_url(); ?><!--"><img src="--><?php //print template_url(); ?><!--assets/img/logo_footer.png" alt=""/></a>-->
                    <small>All Rights Reserved © <?php date('Y'); ?> - Your Brand Ltd.</small>

                </div>

                <div class="col-md-6">
                    <module type="menu" template="simple" data-class="nav nav-center justify-content-center" id="footer_menu" name="footer_menu"/>
                </div>

                <div class="col-md-3 text-center text-md-end">

                    <p class="mb-0"><?php print powered_by_link(); ?></p>
                </div>
            </div>

            <div class="row gap-y">
                <div class="col-12 text-center">
                    <!--                                <p>--><?php //print powered_by_link(); ?><!--</p>-->
                </div>
            </div>
        </div>
    </footer>
<?php endif; ?>
</div>

<button id="to-top" class="btn btn-primary btn-square" style="display: block;"><i class="mdi mdi-arrow-up mdi-18px lh-1 m-0"></i></button>



<script src="<?php print template_url(); ?>dist/main.min.js"></script>

<?php include('footer_cart.php'); ?>



<?php $collaplseNav_file = normalize_path( mw_includes_path().'api/libs/collapse-nav/dist/collapseNav.js',false);   ?>
<?php if(is_file($collaplseNav_file)){ ?>
    <script>
        // collapse nav
        <?php print  @file_get_contents($collaplseNav_file) ?>
    </script>
<?php } ?>



<?php $collaplseNav_file = normalize_path( mw_includes_path().'api/libs/collapse-nav/dist/collapseNav.css',false);   ?>
<?php if(is_file($collaplseNav_file)){ ?>
    <style>
        // collapse nav
        <?php print  @file_get_contents($collaplseNav_file) ?>
    </style>
<?php } ?>
</body>
</html>
