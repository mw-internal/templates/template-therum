<?php

/*
  type: layout
  content_type: static
  name: Home
  position: 1
  description: Home
*/

?>

<?php include template_dir() . "header.php"; ?>

<div class="edit main-content" data-layout-container rel="content" field="content">
    <module type="layouts" template="content/skin-8"/>
    <module type="layouts" template="content/skin-23"/>
    <module type="layouts" template="content/skin-9"/>
    <module type="layouts" template="content/skin-32"/>


    <module type="layouts" template="call-to-action/skin-18"/>

    <module type="layouts" template="content/skin-25"/>
    <module type="layouts" template="content/skin-43"/>

    <module type="layouts" template="call-to-action/skin-12"/>

    <module type="layouts" template="testimonials/skin-8"/>

    <module type="layouts" template="contacts/skin-5"/>
</div>

<?php include template_dir() . "footer.php"; ?>
